**Predicting restaurant rating and classification based on Zomato Dataset**

**Business Problem**

Description

Restaurants with different cuisine and styles from all over the world can be found here in Sydney. There are so many different types of options are also available for them, either it can be café style or be formal dining.  Sydney is a mecca for the foodies. The number of restaurants are increasing day by day. This industry has not been saturated yet. And new restaurants are opening every day. However, it has become difficult for them to compete with already established restaurants. The key issues that continue to pose a challenge to them include high real estate costs, rising food costs, shortage of quality manpower, fragmented supply chain and over-licensing.

Column Description

•	link contains the URL of the restaurant in the Zomato website.
•	address contains the address of the restaurant in Bengaluru
•	title contains the name of the restaurant
•	rating_number contains the overall rating of the restaurant out of 5
•	votes contain total number of ratings for the restaurant as of the above-mentioned date
•	phone contains the phone number of the restaurant
•	subzone contains the neighbourhood in which the restaurant is located
•	type is the type of restaurants like cafe, quick bites etc
•	rating_text contains the rating type of the restaurant, like good, average, excellent etc. 
•	cuisines food styles, separated by comma
•	cost_2 contains the approximate cost for meal for two people
•	type type of meal the restaurant serves 
•	lng contains the longitude of the restaurant 
•	lat Latitude of the restaurent

Problem Statement
The dataset also contains rating and reviews for each of the restaurant which will help in finding overall rating for the place. So, I will try to predict rating for restaurant based on various features. Also visualize restaurent distributions, cost, and rating. Will make a classification model and compare with other three machine learning model based on accuracy and confusion matrix.

Machine Learning Formulation
Here I suppose to predicted rating of restaurant, so it is basically Regression problem.

Performance Metric
We will try to reduce Mean Square Error i.e. MSE as minimum as possible. So, it is Regression problem reducing MSE. And Ideal MSE is 0

